package Utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadProperties {
public Properties getproperty () throws IOException {

    FileInputStream fileInput = new FileInputStream("src/main/resources/data.Properties");
    Properties  prop = new Properties();
    prop.load(fileInput);
    return prop;


}

}
