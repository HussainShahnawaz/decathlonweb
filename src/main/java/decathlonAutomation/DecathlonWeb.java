package decathlonAutomation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DecathlonWeb extends pageInit{
    public WebDriver driver;

    public DecathlonWeb(WebDriver driver){
        super(driver);
        this.driver=driver;
    }

    @FindBy(xpath = "//div[contains(@class,'btn-close')]")
    WebElement close;

    @FindBy(xpath = "//*[@id=\"8486177\"]/a/div/div/div/div/div/img")
    WebElement product;


    @FindBy(xpath = "//div[@class=' css-1hwfws3']")
    WebElement size;

    @FindBy(xpath = "//div[@class=' css-26l3qy-menu']")
    WebElement size1;

    @FindBy(xpath = "//button[@class='  _3Anj944dj7  btn-yellow-gradient btn-sm        btn']")
    WebElement cart;

    @FindBy(xpath = "//div[@class='_1tGY6eWW63 _3Nfpuksh7a']/span[1]")
    WebElement disc;

    @FindBy(xpath = "//div[@class='_1tGY6eWW63 _3Nfpuksh7a']/span[3]")
    WebElement price;

    public void closepage(){

        close.click();
    }

    public void selecrProduct(){

        product.click();
    }

    public void openDropdown(){

        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(size));
       size.click();

    }

    public void selectSize(){

        new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOf(size1));
        size1.click();
    }

    public void addtoCart(){
        cart.click();
    }

    public String discount(){
        return disc.getText();
    }

    public String actualPrice(){

        return price.getText();
    }



}
