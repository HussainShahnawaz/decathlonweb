package DecathlonWebTest;

import Utility.BrowserSetup;
import Utility.ReadProperties;
import decathlonAutomation.DecathlonWeb;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ValidateTest {

    public static WebDriver driver;
    Properties prop;


    @BeforeMethod
    //Setting the browser
    public void BrowserSetup() throws IOException {
        BrowserSetup BS = new BrowserSetup();
        driver = BS.Browsersetup();
        ReadProperties rp = new ReadProperties();
        prop = rp.getproperty();
    }


    @Test
    public void runTest() {
        DecathlonWeb Web = new DecathlonWeb(driver);
        Web.closepage();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Web.selecrProduct();
        String Discount = Web.discount();
        String price = Web.actualPrice();
        int Dis = Integer.parseInt(Discount.replaceAll( "[^\\d]",""));

        int Totalprice = Integer.parseInt(price.replaceAll( "[^\\d]",""));

        Assert.assertTrue(Dis<Totalprice);



    }
}
